<?php  get_header(); ?>
    <div class="uk-container uk-margin-large-bottom">
        <div class="uk-margin-large-top search-result-title uk-text-center">
            <h1>404 NOT FOUND</h1>
        </div>
        <div class="uk-text-center">
            <img src="<?php echo get_template_directory_uri() .'/assets/images/no-data.webp'; ?>" />
            <p class="uk-margin-medium-bottom">
                <?php if (get_bloginfo("language") == 'mn') {
                ?>
                    Уучлаарай, Мэдээлэл олдсонгүй!
                <?php
                } else {
                    Sorry, Not found data!
                } ?>
            </p>
        </div>
    </div>
<?php get_footer(); ?>
