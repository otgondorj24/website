<?php 
add_action( 'wp_ajax_getLocation', 'getLocation' );
add_action( 'wp_ajax_nopriv_getLocation', 'getLocation' );
function getLocation() {
    $body = [
        'aimag_name'  => '',
        'sum_name' => '',
    ];
    $body = wp_json_encode( $body );
    $response = wp_remote_post( 'https://iamkiosk.gerege.mn/kiosk_core/make/request', array(
        'body'    => $body,
        'headers' => array(
            'Content-Type' => 'application/json',
            'message_code' => 2204,
        ),
    ) );

    echo json_encode($response['body']);
    exit;
}