$(document).ready(function() {
    setTimeout(() => {
      $('#loader').addClass('uk-hidden');
    }, 1000);
	
	var sections = $('.order-scroll')
      , nav = $('.order-container')
      , nav_height = nav.outerHeight();

    $(window).on('scroll', function () {
      var cur_pos = $(this).scrollTop();
      
      sections.each(function() {
        var top = $(this).offset().top - nav_height,
            bottom = top + $(this).outerHeight();
        
        if (cur_pos >= top && cur_pos <= bottom) {
          nav.find('a').removeClass('active');
          sections.removeClass('active');
          
          $(this).addClass('active');
          nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
        }
      });
    });

    nav.find('a').on('click', function () {
      var $el = $(this)
        , id = $el.attr('href');
      
      $('html, body').animate({
        scrollTop: $(id).offset().top - 150
      }, 500);
      
      return false;
    });

    var _content = $('.sidemodal');
    $('.mod-btn .bt').on('click',function() {
        let _this = $(this);
        if (_this.hasClass('active')) {
            setTimeout(() => {
                _this.removeClass('active');
                _content.removeClass('full-line');
                _content.removeAttr('style');
            }, 400,backPush());
        }
        else {
            _content.attr('style','z-index:1000');
            setTimeout(() => {
                _content.addClass('open')
                _this.addClass('active');
                $('.logo').addClass('push');
            }, 400,_content.addClass('full-line'));
        }
    });
    function backPush() {
        $('.logo').removeClass('push');
        _content.removeClass('open');
    }

    $(window).scroll(function(){
        if ($(window).scrollTop()>0 && _content.hasClass('open')) {
            let _this = $('.mod-btn .bt');
            setTimeout(() => {
                _this.removeClass('active');
                _content.removeClass('full-line');
                _content.removeAttr('style');
            }, 400,backPush());
        }
    })


    $('.gerege-testimonial .person').on('click',function() {

        var _picture = $('.gerege-testimonial .thumbnail');
        var _name = $('.gerege-testimonial .content h5');
        var _position = $('.gerege-testimonial .content h6');
        var _desc = $('.gerege-testimonial .content p');

        $('.gerege-testimonial .person').removeClass('active');

        _picture.attr('style','background-image:url('+$(this).attr('data-image')+')');
        _name.text($(this).attr('data-name'));
        _position.text($(this).attr('data-position'));
        _desc.text($(this).attr('data-description'));

        $(this).addClass('active');
        
    });

});

$('.two-column .sub-menu').addClass('uk-column-1-2 uk-column-divider');


function initMap() {
    const uluru = { lat: Number($('#gerege-map').attr('data-latitude')), lng: Number($('#gerege-map').attr('data-longitude')) };
    const map = new google.maps.Map(document.getElementById("gerege-map"), {
      zoom: 17,
      center: uluru,
    });
    const marker = new google.maps.Marker({
      position: uluru,
      map: map,
      icon: $('#gerege-map').attr('data-icon')
    });

    map.setOptions({
        styles: [
            {
              elementType: "geometry",
              stylers: [{ color: "#E7ECF3" }],
            },
            {
              elementType: "labels.text.fill",
              stylers: [{ color: "#616161" }],
            },
            {
              elementType: "labels.text.stroke",
              stylers: [{ color: "#f5f5f5" }],
            },
            {
              featureType: "administrative.land_parcel",
              elementType: "labels.text.fill",
              stylers: [{ color: "#bdbdbd" }],
            },
            {
              featureType: "poi",
              elementType: "geometry",
              stylers: [{ color: "#eeeeee" }],
            },
            {
              featureType: "poi",
              elementType: "labels.text.fill",
              stylers: [{ color: "#757575" }],
            },
            {
              featureType: "poi.park",
              elementType: "geometry",
              stylers: [{ color: "#e5e5e5" }],
            },
            {
              featureType: "poi.park",
              elementType: "labels.text.fill",
              stylers: [{ color: "#9e9e9e" }],
            },
            {
              featureType: "road",
              elementType: "geometry",
              stylers: [{ color: "#ffffff" }],
            },
            {
              featureType: "road.arterial",
              elementType: "labels.text.fill",
              stylers: [{ color: "#757575" }],
            },
            {
              featureType: "road.highway",
              elementType: "geometry",
              stylers: [{ color: "#dadada" }],
            },
            {
              featureType: "road.highway",
              elementType: "labels.text.fill",
              stylers: [{ color: "#616161" }],
            },
            {
              featureType: "road.local",
              elementType: "labels.text.fill",
              stylers: [{ color: "#9e9e9e" }],
            },
            {
              featureType: "transit.line",
              elementType: "geometry",
              stylers: [{ color: "#e5e5e5" }],
            },
            {
              featureType: "transit.station",
              elementType: "geometry",
              stylers: [{ color: "#eeeeee" }],
            },
            {
              featureType: "water",
              elementType: "geometry",
              stylers: [{ color: "#c9c9c9" }],
            },
            {
              featureType: "water",
              elementType: "labels.text.fill",
              stylers: [{ color: "#9e9e9e" }],
            },
          ],
    });
  }

  var ajaxurl = window.location.protocol + "//" + window.location.host + "/wordpress/wp-admin/admin-ajax.php";


  function KioskinitMap() {

    const uluru = { lat: 47.9141423, lng: 106.9184307 };
    const map = new google.maps.Map(document.getElementById("geregekmap"), {
      zoom: 13,
      center: uluru,
    });

    map.setOptions({
        styles: [
            {
              elementType: "geometry",
              stylers: [{ color: "#E7ECF3" }],
            },
            {
              elementType: "labels.text.fill",
              stylers: [{ color: "#616161" }],
            },
            {
              elementType: "labels.text.stroke",
              stylers: [{ color: "#f5f5f5" }],
            },
            {
              featureType: "administrative.land_parcel",
              elementType: "labels.text.fill",
              stylers: [{ color: "#bdbdbd" }],
            },
            {
              featureType: "poi",
              elementType: "geometry",
              stylers: [{ color: "#eeeeee" }],
            },
            {
              featureType: "poi",
              elementType: "labels.text.fill",
              stylers: [{ color: "#757575" }],
            },
            {
              featureType: "poi.park",
              elementType: "geometry",
              stylers: [{ color: "#e5e5e5" }],
            },
            {
              featureType: "poi.park",
              elementType: "labels.text.fill",
              stylers: [{ color: "#9e9e9e" }],
            },
            {
              featureType: "road",
              elementType: "geometry",
              stylers: [{ color: "#ffffff" }],
            },
            {
              featureType: "road.arterial",
              elementType: "labels.text.fill",
              stylers: [{ color: "#757575" }],
            },
            {
              featureType: "road.highway",
              elementType: "geometry",
              stylers: [{ color: "#dadada" }],
            },
            {
              featureType: "road.highway",
              elementType: "labels.text.fill",
              stylers: [{ color: "#616161" }],
            },
            {
              featureType: "road.local",
              elementType: "labels.text.fill",
              stylers: [{ color: "#9e9e9e" }],
            },
            {
              featureType: "transit.line",
              elementType: "geometry",
              stylers: [{ color: "#e5e5e5" }],
            },
            {
              featureType: "transit.station",
              elementType: "geometry",
              stylers: [{ color: "#eeeeee" }],
            },
            {
              featureType: "water",
              elementType: "geometry",
              stylers: [{ color: "#c9c9c9" }],
            },
            {
              featureType: "water",
              elementType: "labels.text.fill",
              stylers: [{ color: "#9e9e9e" }],
            },
          ],
    });

    jQuery.ajax({
        url : ajaxurl,
        data : { 
            action: "getLocation",
        },
        dataType: 'json',
        type: 'post',
        success: function(response) {
            let data = JSON.parse(response).result;
            data.forEach(element => {
                const marker = new google.maps.Marker({
                    position: {
                        lat: Number(element['latitude']),
                        lng: Number(element['longitude'])
                    },
                    map: map,
                    icon: $('#geregekmap').attr('data-icon')
                });

                let contentString = '<div class="map-info">';

                contentString += '<div class="thumb" style="background-image:url(https://app-backend.gerege.mn/file/?file='+element["photo"]+')"></div>';

                contentString += '<h4>'+element['location_name']+'</h4>';

                contentString += '<p>'+element['location_info']+'</p>';
                
                contentString += '<h6>Цагийн хуваарь:'+element['timetable']+'</h6>';

                contentString += '</div>';

                const infowindow = new google.maps.InfoWindow({
                    content: contentString,
                    maxWidth: 500
                });

                marker.addListener("click", () => {
                    infowindow.open({
                      anchor: marker,
                      map,
                      shouldFocus: false,
                    });
                });
            });

        }
    })

  }

  if ($('#geregekmap').length) window.initMap = KioskinitMap;
  if ($('#gerege-map').length) window.initMap = initMap;
 