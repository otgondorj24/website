<?php 

if ( ! class_exists( 'gerege_timeline_Shortcode' ) ) {

    class gerege_timeline_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_timeline', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_timeline', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_timeline', $atts );

            $items = vc_param_group_parse_atts($atts['item']);

            $class = $atts['class'];

            $content = '';

            $output = sprintf("<div class='gerege-timeline %s'><div class='uk-grid uk-grid-match uk-grid-collapse' uk-grid>",$class);

            for ($i=0; $i < count($items); $i++) {

                $url = isset($items[$i]['picture']) ? wp_get_attachment_image_src( $items[$i]['picture'], 'full', '' )[0] : '';

                $content .= '<div class="uk-width-1-1 uk-hidden@m">
                    <div class="uk-gird uk-grid-small" uk-grid>
                        <div class="uk-width-1-3">
                            <div class="year uk-flex uk-flex-center">
                                <div class="uk-flex uk-flex-middle uk-flex-center">
                                    <div class="uk-flex uk-flex-middle">
                                        <div>
                                            <h5>'.$items[$i]['year'].'</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <h5>'.$items[$i]['title'].'</h5>
                            <div class="thumb" style="background-image:url('.$url.')"></div>
                            <p>'.$items[$i]['desc'].'</p>
                        </div>
                    </div>
                </div>';

                if ($i == 0) {
                    $output .= '<div class="uk-width-2-5 uk-visible@m"></div>
                                <div class="uk-width-1-5 uk-visible@m">
                                    <div class="year uk-flex uk-flex-center">
                                        <div class="uk-flex uk-flex-middle uk-flex-center">
                                            <div class="uk-flex uk-flex-middle">
                                                <div>
                                                    <h5>'.$items[$i]['year'].'</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-2-5 uk-visible@m">
                                    <div class="uk-flex uk-flex-middle">
                                        <div class="uk-width-expand">
                                            <h5>'.$items[$i]['title'].'</h5>
                                            <p>'.$items[$i]['desc'].'</p>
                                        </div>
                                        <div class="uk-width-auto">
                                            <div class="thumb uk-margin-left" style="background-image:url('.$url.')"></div>
                                        </div>
                                    </div>
                                </div>';
                } else {
                    if ($i % 2 ==0) {
                        $output .= '<div class="uk-width-2-5 uk-visible@m"></div>
                                    <div class="uk-width-1-5 uk-visible@m">
                                        <div class="year uk-flex uk-flex-center">
                                            <div class="uk-flex uk-flex-middle uk-flex-center">
                                                <div class="uk-flex uk-flex-middle">
                                                    <div>
                                                        <h5>'.$items[$i]['year'].'</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uk-width-2-5 uk-visible@m">
                                        <div class="uk-flex uk-flex-middle">
                                            <div class="uk-width-expand">
                                                <h5>'.$items[$i]['title'].'</h5>
                                                <p>'.$items[$i]['desc'].'</p>
                                            </div>
                                            <div class="uk-width-auto">
                                                <div class="thumb uk-margin-left" style="background-image:url('.$url.')"></div>
                                            </div>
                                        </div>
                                    </div>';
                    } else {
                        $output .= '<div class="uk-width-2-5 uk-visible@m">
                                        <div class="uk-flex uk-flex-middle">
                                            <div class="uk-width-auto">
                                                <div class="thumb uk-margin-right" style="background-image:url('.$url.')"></div>
                                            </div>
                                            <div class="uk-width-expand">
                                                <h5>'.$items[$i]['title'].'</h5>
                                                <p>'.$items[$i]['desc'].'</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uk-width-1-5 uk-visible@m">
                                        <div class="year uk-flex uk-flex-center">
                                            <div class="uk-flex uk-flex-middle uk-flex-center">
                                                <div class="uk-flex uk-flex-middle">
                                                    <div>
                                                        <h5>'.$items[$i]['year'].'</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uk-width-2-5 uk-visible@m"></div>';
                    }
                }
                

            }

            $output .= $content;

            $output .= '<div class="uk-width-1-1 height70"></div></div></div>';

            return $output;

        }


 
        public static function map() {
            return array(
                'name'        => esc_html__( 'Түүхэн амжилт', 'gerege' ),
                'description' => esc_html__( 'Гэрэгэ компаний түүхэн амжилт', 'gerege' ),
                'base'        => 'gerege_timeline',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'item',
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "heading" => __( "Picture", "gerege" ),
                                "param_name" => "picture",
                            ),
                            array(
                                'type' => 'textfield',
                                'value' => '',
                                'heading' => 'Year',
                                'param_name' => 'year',
                            ),
                            array(
                                'type' => 'textfield',
                                'value' => '',
                                'heading' => 'Title',
                                'param_name' => 'title',
                            ),
                            array(
                                'type' => 'textarea',
                                'value' => '',
                                'heading' => 'Descripton',
                                'param_name' => 'desc',
                            )
                        )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Extra Class", "gerege" ),
                        "param_name" => "class",
                    ),
                ),
            );
        }

    }

}
new gerege_timeline_Shortcode;
