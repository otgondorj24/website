<?php 

if ( ! class_exists( 'gerege_video_Shortcode' ) ) {

    class gerege_video_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_video', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_video', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_video', $atts );

            $url = wp_get_attachment_image_src( $atts['image'], 'full', '' )[0];

            $video = $atts['video_url'];

            $play_btn = get_template_directory_uri() . '/assets/images/play-btn.svg';

            $class = $atts['class'];

            $output = sprintf("
                <div class='video-player %s'>
                    <div class='uk-position-relative' >
                        <img src='%s' />
                        <div class='uk-position-center uk-position-z-index'>
                            <a href='#modal-media-youtube' uk-toggle><img src='%s' class='play-btn' id='video-modal' /></a>
                        </div>
                    </div>
                </div>
            ",
            $class,
            $url,
            $play_btn
            );

            $output .= sprintf('
            <div id="modal-media-youtube" class="uk-flex-middle" uk-modal>
                <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                    <button class="uk-modal-close-outside" type="button" uk-close></button>
                    <iframe src="%s" width="950" height="550" uk-video uk-responsive></iframe>
                </div>
            </div>
            ', $video);

            return $output;

        }


        public static function map() {
            return array(
                'name'        => esc_html__( 'Видёо', 'gerege' ),
                'description' => esc_html__( 'Модал видёо тоглуулагч', 'gerege' ),
                'base'        => 'gerege_video',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                        "type" => "attach_image",
                        "heading" => __( "Background image", "gerege" ),
                        "param_name" => "image",
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Youtube Embed URL", "gerege" ),
                        "param_name" => "video_url",
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Extra Class", "blank" ),
                        "param_name" => "class",
                    )
                ),
            );
        }

    }

}
new gerege_video_Shortcode;