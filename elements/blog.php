<?php 

if ( ! class_exists( 'gerege_blog_Shortcode' ) ) {


    class gerege_blog_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_blog', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_blog', __CLASS__ . '::map' );
            }

        }

        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_blog', $atts );

            $class = $atts['class'];

            $the_query = new WP_Query( array( 
                'category_name' => $atts['cat'], 
                'posts_per_page' => 6 
            ) ); 

            $output = "<div class='uk-grid uk-position-relative uk-margin-top uk-child-width-1-3@m z-10' uk-grid >";

            if ( $the_query->have_posts() ) {
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    
                    $output .="<div class='gerege-post'>";
                    $output .="<div class='thumbnail' style='background-image:url(".get_the_post_thumbnail_url(get_the_ID(),'full').")'></div>";
                    $output .="<h4>".get_the_title()."</h4>";
                    $output .="<div class='uk-margin-medium-top uk-grid uk-child-width-1-2' uk-grid>";
                    $output .="<div><a href='".get_the_permalink()."'><button class='primary-button'>";
                    if (get_bloginfo("language") == 'mn') $output .= 'Дэлгэрэнгүй';
                    else $output .= 'Read more';
                    $output .="</button></a></div>";
                    $output .="<div class='uk-text-right date'>".get_the_date( 'Y/m/d', get_the_ID() )."</div>";
                    $output .="</div>";
                    $output .="</div>";
                }
            }

            $output .= "</div>";

            wp_reset_postdata();
           
            return $output;

        }


        public static function map() {

            $args = array(
                'type'                     => 'post',
                'child_of'                 => 0,
                'parent'                   => '',
                'orderby'                  => 'name',
                'order'                    => 'ASC',
                'hide_empty'               => 1,
                'hierarchical'             => 1,
                'exclude'                  => '',
                'include'                  => '',
                'number'                   => '',
                'taxonomy'                 => 'category',
                'pad_counts'               => false 
            );
            
            $category = array();
            
            $categories = get_categories( $args );
            
            foreach ($categories as $value) 
            $category[$value->name] = $value->name;
            
            return array(
                'name'        => esc_html__( 'Мэдээ мэдээлэл', 'gerege' ),
                'description' => esc_html__( 'Вэб сайтын блог мэдээлэл', 'gerege' ),
                'base'        => 'gerege_blog',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                        'type'          => 'dropdown',
                        'heading'       => __( 'Category', 'gerege' ),
                        'value'         =>  $category,
                        'param_name'    => 'cat'
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Extra Class", "gerege" ),
                        "param_name" => "class",
                    ),
                ),
            );
        }

    }

}
new gerege_blog_Shortcode;