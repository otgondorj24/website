<?php 

if ( ! class_exists( 'gerege_testimonial_Shortcode' ) ) {

    class gerege_testimonial_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_testimonial', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_testimonial', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_testimonial', $atts );

            $items = vc_param_group_parse_atts($atts['item']);

            $class = $atts['class'];


            $output = sprintf("<div class='gerege-testimonial uk-visible@s uk-flex %s'>",$class);

            $url = isset($items[0]['picture']) ? wp_get_attachment_image_src( $items[0]['picture'], 'full', '' )[0] : '';

            $name = $items[0]['name'] ? $items[0]['name'] : '';

            $desc = $items[0]['desc'] ? $items[0]['desc'] : '';

            $position = $items[0]['position'] ? $items[0]['position'] : '';

            $output .= '<div class="uk-width-1-5"><div class="thumbnail" style="background-image:url('.$url.')"></div></div>';
            $output .= '<div class="uk-width-expand uk-background-default">
                <div class="content">
                    <h5>'.$name.'</h5>
                    <h6>'.$position.'</h6>
                    <p>'.$desc.'</p>
                </div>
            </div>';
            $output .= '<div class="uk-width-1-5 uk-flex uk-flex-middle"><div class="uk-margin-small-left uk-grid uk-child-width-1-2" uk-grid>';

            for ($i=0; $i < count($items); $i++) {

                $url = isset($items[$i]['picture']) ? wp_get_attachment_image_src( $items[$i]['picture'], 'full', '' )[0] : '';

                $name = $items[$i]['name'];

                $desc = $items[$i]['desc'];

                $position = $items[$i]['position'];

                if ( $i==0 ) $active = 'active';
                else $active = '';

                $output .='<div class="person '.$active.'" data-name="'.$name.'" data-description="'.$desc.'" data-position="'.$position.'" data-image="'.$url.'">
                    <div class="image" style="background-image:url('.$url.')"></div>
                </div>';

            }

            $output .='</div></div>';

            $output .= '</div>';

            $output .= sprintf("<div class='gerege-testimonial uk-hidden@m %s' style='height:max-content'>",$class);

            $url = isset($items[0]['picture']) ? wp_get_attachment_image_src( $items[0]['picture'], 'full', '' )[0] : '';

            $name = $items[0]['name'] ? $items[0]['name'] : '';

            $desc = $items[0]['desc'] ? $items[0]['desc'] : '';

            $position = $items[0]['position'] ? $items[0]['position'] : '';

            $output .= '<div class="uk-width-1-1"><div class="thumbnail" style="background-image:url('.$url.');"></div></div>';
            $output .= '<div class="uk-width-1-1 uk-background-default">
                <div class="content">
                    <h5>'.$name.'</h5>
                    <h6>'.$position.'</h6>
                    <p>'.$desc.'</p>
                </div>
            </div>';

            

            $output .= '<div class="uk-grid uk-child-width-1-3 uk-padding" uk-grid>';

            for ($i=0; $i < count($items); $i++) {

                $url = isset($items[$i]['picture']) ? wp_get_attachment_image_src( $items[$i]['picture'], 'full', '' )[0] : '';

                $name = $items[$i]['name'];

                $desc = $items[$i]['desc'];

                $position = $items[$i]['position'];

                if ( $i==0 ) $active = 'active';
                else $active = '';

                $output .='<div class="person '.$active.'" data-name="'.$name.'" data-description="'.$desc.'" data-position="'.$position.'" data-image="'.$url.'">
                    <div class="image" style="background-image:url('.$url.')"></div>
                </div>';

            }

            $output .= '</div>';

            $output .= '</div>';

            return $output;

        }


 
        public static function map() {
            return array(
                'name'        => esc_html__( 'Хамт олны сэтгэгдэл', 'gerege' ),
                'description' => esc_html__( 'Гэрэгэ компаний хамт олны сэтгэгдэл', 'gerege' ),
                'base'        => 'gerege_testimonial',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'item',
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "heading" => __( "Picture", "gerege" ),
                                "param_name" => "picture",
                            ),
                            array(
                                'type' => 'textfield',
                                'value' => '',
                                'heading' => 'Name',
                                'param_name' => 'name',
                            ),
                            array(
                                'type' => 'textfield',
                                'value' => '',
                                'heading' => 'Position',
                                'param_name' => 'position',
                            ),
                            array(
                                'type' => 'textarea',
                                'value' => '',
                                'heading' => 'Descripton',
                                'param_name' => 'desc',
                            )
                        )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Extra Class", "gerege" ),
                        "param_name" => "class",
                    ),
                ),
            );
        }

    }

}
new gerege_testimonial_Shortcode;
