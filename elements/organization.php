<?php 

if ( ! class_exists( 'gerege_orginization_Shortcode' ) ) {

    class gerege_orginization_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_orginization', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_orginization', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_orginization', $atts );

            $class = $atts['class'];

            $items = vc_param_group_parse_atts($atts['item']);

            $icon = get_template_directory_uri() .'/assets/images/mark.png';

            $output = sprintf("<div class='uk-grid sub-orginization uk-child-width-1-4@m %s' uk-grid>",$class);

            for ($i=0; $i < count($items); $i++) {

                $link = vc_build_link($items[$i]['button']);

                $url = wp_get_attachment_image_src( $items[$i]['image'], 'full', '' )[0];

                $output .= sprintf('
                    <div class="uk-text-center">
                        <div class="thumbnail" style="background-image:url(%s);"></div>
                        <div class="mark"><img src="%s" /></div>
                        <h4 class="title">%s</h4>
                        <p>%s</p>
                        <div class="uk-text-center">
                            <a href="%s"><button class="primary-button">%s</button></a>
                        </div>
                    </div>
                ',$url,$icon,$items[$i]['title'],$items[$i]['desc'],$link['url'],$link['title']);
            }

            $output .= '</div>';

            return $output;

        }


 
        public static function map() {
            return array(
                'name'        => esc_html__( 'Салбар байгууллага', 'gerege' ),
                'description' => esc_html__( 'Салбар байгууллагийн мэдээлэл', 'gerege' ),
                'base'        => 'gerege_orginization',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                    'type' => 'param_group',
                    'value' => '',
                    'param_name' => 'item',
                    'params' => array(
                        array(
                            'type' => 'textfield',
                            'value' => '',
                            'heading' => 'Title',
                            'param_name' => 'title',
                        ),
                        array(
                            'type' => 'textarea',
                            'value' => '',
                            'heading' => 'Descripton',
                            'param_name' => 'desc',
                        ),
                        array(
                            "type" => "attach_image",
                            "heading" => __( "Image", "gerege" ),
                            "param_name" => "image",
                        ),
                        array(
                            'type'       => 'vc_link',
                            'heading'    => esc_html__( 'button', 'gerege' ),
                            'param_name' => 'button',
                            'value'      => array()
                        )
                    )
                ),
                array(
                    "type" => "textfield",
                    "heading" => __( "Extra Class", "gerege" ),
                    "param_name" => "class",
                ),
            ),
            );
        }

    }

}
new gerege_orginization_Shortcode;
