<?php 

if ( ! class_exists( 'gerege_workspace_Shortcode' ) ) {

    class gerege_workspace_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_workspace', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_workspace', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_workspace', $atts );

            $link = vc_build_link($atts['button']);

            $title = $atts['title'];

            $company = $atts['company'];

            $desc = $atts['desc'];

            $button = sprintf("<a href='%s'><button class='secondary-button uk-margin-small-left'>%s</button></a>",$link['url'],$link['title']);

            $output = '<div class="uk-grid gerege-workspace" uk-grid>
                <div class="uk-width-2-3@m">
                    <h4>'.$title.'</h4>
                    <h6>'.$company.'</h6>
                </div>
                <div class="uk-width-1-3@m uk-text-right padding-top"><span class="icon" uk-icon="icon: bookmark; ratio: 0.9"></span>'.$button.'</div>
                <div class="uk-width-1-1@m"><p>'.$desc.'</p></div>
            </div>';

            

            return $output;

        }


        public static function map() {
            return array(
                'name'        => esc_html__( 'Ажлын байр', 'gerege' ),
                'description' => esc_html__( 'Ажлын зар оруулах модул', 'gerege' ),
                'base'        => 'gerege_workspace',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Title',
                        'param_name' => 'title',
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Company',
                        'param_name' => 'company',
                    ),
                    array(
                        'type' => 'textarea',
                        'value' => '',
                        'heading' => 'Description',
                        'param_name' => 'desc',
                    ),
                    array(
                        'type'       => 'vc_link',
                        'heading'    => esc_html__( 'Button', 'gerege' ),
                        'param_name' => 'button',
                        'value'      => array()
                    ),
                ),
            );
        }

    }

}
new gerege_workspace_Shortcode;