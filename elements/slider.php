<?php 

if ( ! class_exists( 'gerege_carousel_Shortcode' ) ) {

    class gerege_carousel_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_carousel', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_carousel', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_carousel', $atts );

            $class = $atts['class'];

            $items = vc_param_group_parse_atts($atts['item']);

            $mlist = vc_param_group_parse_atts($atts['sidebar']);

            $icon = get_template_directory_uri() .'/assets/images/mark.png';

            $output = sprintf("<div class='home-carousel %s'>",$class);

            $modal = '';

            $modal .= '<div class="sidemodal"><div class="uk-padding-large"><ul class="list">';

            for ($i=0; $i < count($mlist); $i++) {

                $link = vc_build_link($mlist[$i]['button']);
                $modal .= '<li>';
                $modal .= '<h3>'.$mlist[$i]['title'].'</h3>';
                $modal .= '<p>'.$mlist[$i]['desc'].'</p>';
                $modal .= '<a href="'.$link['url'].'" class="perma">'.$link['title'].'</a>';
                $modal .= '</li>';
            }

            $modal .= '</ul></div><div class="mod-btn"><div class="uk-position-relative"><span class="bt" uk-icon="icon: chevron-right; ratio: 2"></span>';

            $modal .= '<img src="'.get_template_directory_uri().'/assets/images/sidebtntback.png'.'" />';

            $modal .= '</div></div></div>';

            $output .= $modal;

            $output .= '<div class="uk-position-relative" tabindex="-1" uk-slideshow="animation: fade"><ul class="uk-slideshow-items" >';

            for ($i=0; $i < count($items); $i++) {

                $url = wp_get_attachment_image_src( $items[$i]['image'], 'full', '' )[0];
                $output .= '<li>';
                $output .= '<img src="'.$url.'" alt="" />';
                $output .= '</li>';
            }

            $output .= '</ul>';

            $link = vc_build_link($atts['button']);
            $output .= '<div class="content uk-position-center-left m15per w600">';
            $output .= '<h1>'.$atts['title'].'</h1>';
            $output .= '<p>'.$atts['desc'].'</p>';
            $output .= '<a href="'.$link["url"].'" class="perma">'.$link["title"].'</a>';
            $output .= '</div>';
            $output .= '<div class="pagination"><ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul></div></div>';

            $output .= '</div>';

            return $output;

        }


 
        public static function map() {
            return array(
                'name'        => esc_html__( 'Слайд', 'gerege' ),
                'description' => esc_html__( 'Нүүр хуудас дээрх бүтэн слайд', 'gerege' ),
                'base'        => 'gerege_carousel',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                    'type' => 'param_group',
                    'value' => '',
                    'heading' => 'Carousel Images',
                    'param_name' => 'item',
                    'params' => array(
                        array(
                            "type" => "attach_image",
                            "heading" => __( "Image", "gerege" ),
                            "param_name" => "image",
                        )
                    )
                ),
                array(
                    'type' => 'textfield',
                    'value' => '',
                    'heading' => 'Title',
                    'param_name' => 'title',
                ),
                array(
                    'type' => 'textarea',
                    'value' => '',
                    'heading' => 'Descripton',
                    'param_name' => 'desc',
                ),
                array(
                    'type'       => 'vc_link',
                    'heading'    => esc_html__( 'button', 'gerege' ),
                    'param_name' => 'button',
                    'value'      => array()
                ),
                array(
                    'type' => 'param_group',
                    'value' => '',
                    'param_name' => 'sidebar',
                    'heading' => 'Sidebar Items',
                    'params' => array(
                        array(
                            'type' => 'textfield',
                            'value' => '',
                            'heading' => 'Title',
                            'param_name' => 'title',
                        ),
                        array(
                            'type' => 'textarea',
                            'value' => '',
                            'heading' => 'Descripton',
                            'param_name' => 'desc',
                        ),
                        array(
                            'type'       => 'vc_link',
                            'heading'    => esc_html__( 'button', 'gerege' ),
                            'param_name' => 'button',
                            'value'      => array()
                        ),
                    )
                ),
                array(
                    "type" => "textfield",
                    "heading" => __( "Extra Class", "gerege" ),
                    "param_name" => "class",
                ),
            ),
            );
        }

    }

}
new gerege_carousel_Shortcode;
