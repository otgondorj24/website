<?php 

if ( ! class_exists( 'gerege_button_Shortcode' ) ) {

    class gerege_button_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_button', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_button', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_button', $atts );

            $link = vc_build_link($atts['button']);

            $class = $atts['class'];

            $output = sprintf("<div class='%s'><a href='%s'><button class='primary-button'>%s</button></a></div>",$class,$link['url'],$link['title']);

            return $output;

        }


        public static function map() {
            return array(
                'name'        => esc_html__( 'Товч', 'gerege' ),
                'description' => esc_html__( 'Үндсэн товч', 'gerege' ),
                'base'        => 'gerege_button',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                        'type'       => 'vc_link',
                        'heading'    => esc_html__( 'URL', 'gerege' ),
                        'param_name' => 'button',
                        'value'      => array()
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Extra Class", "blank" ),
                        "param_name" => "class",
                    )
                ),
            );
        }

    }

}
new gerege_button_Shortcode;