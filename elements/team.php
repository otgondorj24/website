<?php 

if ( ! class_exists( 'gerege_team_Shortcode' ) ) {

    class gerege_team_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_team', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_team', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_team', $atts );

            $items = vc_param_group_parse_atts($atts['item']);

            $class = $atts['class'];


            $output = sprintf("<div class='gerege-team %s'><div class='uk-grid uk-child-width-1-3@m' uk-grid>",$class);

            for ($i=0; $i < count($items); $i++) {

                $url = isset($items[$i]['picture']) ? wp_get_attachment_image_src( $items[$i]['picture'], 'full', '' )[0] : '';

                $output .= '<div><div class="item">';

                $output .= '<div class="thumb" style="background-image:url('.$url.')"></div>';

                $output .= '<div>
                    <h4>'.$items[$i]['name'].'</h4>
                    <p>'.$items[$i]['desc'].'</p>
                </div>';

                $output .= '</div></div>';

            }

            $output .= '</div></div>';

            return $output;

        }


 
        public static function map() {
            return array(
                'name'        => esc_html__( 'Багийн гишүүд', 'gerege' ),
                'description' => esc_html__( 'Гэрэгэ багын гишүүд', 'gerege' ),
                'base'        => 'gerege_team',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'item',
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "heading" => __( "Picture", "gerege" ),
                                "param_name" => "picture",
                            ),
                            array(
                                'type' => 'textfield',
                                'value' => '',
                                'heading' => 'Name',
                                'param_name' => 'name',
                            ),
                            array(
                                'type' => 'textarea',
                                'value' => '',
                                'heading' => 'Descripton',
                                'param_name' => 'desc',
                            )
                        )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Extra Class", "gerege" ),
                        "param_name" => "class",
                    ),
                ),
            );
        }

    }

}
new gerege_team_Shortcode;
