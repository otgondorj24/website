<?php 

if ( ! class_exists( 'gerege_map_Shortcode' ) ) {

    class gerege_map_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_map', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_map', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_map', $atts );

            $url = $atts['image'] ? wp_get_attachment_image_src( $atts['image'], 'full', '' )[0] : '';

            $class = $atts['class'];

            $output = '<div id="gerege-map" data-icon="'.$url.'" data-latitude="'.$atts["latitude"].'" data-longitude="'.$atts["longitude"].'" style="height:'.$atts["height"].'px;"></div>';

            return $output;

        }


        public static function map() {
            return array(
                'name'        => esc_html__( 'Газарын зураг', 'gerege' ),
                'description' => esc_html__( 'Google map', 'gerege' ),
                'base'        => 'gerege_map',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                        "type" => "attach_image",
                        "heading" => __( "Location mark", "gerege" ),
                        "param_name" => "image",
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Height", "gerege" ),
                        "param_name" => "height",
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Longitude", "gerege" ),
                        "param_name" => "longitude",
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Latitude", "gerege" ),
                        "param_name" => "latitude",
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Extra Class", "blank" ),
                        "param_name" => "class",
                    )
                ),
            );
        }

    }

}
new gerege_map_Shortcode;