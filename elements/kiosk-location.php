<?php 

if ( ! class_exists( 'gerege_location_Shortcode' ) ) {

    class gerege_location_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_location', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_location', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_location', $atts );

            $url = $atts['icon'] ? wp_get_attachment_image_src( $atts['icon'], 'full', '' )[0] : '';

            $class = $atts['class'];

            $output = '<div id="geregekmap" data-icon="'.$url.'" style="height:'.$atts["height"].'px;"></div>';

            return $output;

        }


 
        public static function map() {
            return array(
                'name'        => esc_html__( 'Киоск байршил', 'gerege' ),
                'description' => esc_html__( 'Гэрэгэ киоск байршил', 'gerege' ),
                'base'        => 'gerege_location',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                        "type" => "attach_image",
                        "heading" => __( "Icon", "gerege" ),
                        "param_name" => "icon",
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Height", "gerege" ),
                        "param_name" => "height",
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Extra Class", "gerege" ),
                        "param_name" => "class",
                    ),
                ),
            );
        }

    }

}
new gerege_location_Shortcode;
