<?php 

if ( ! class_exists( 'gerege_product_Shortcode' ) ) {

    class gerege_product_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_product', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_product', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_product', $atts );

            $class = $atts['class'];

            $items = vc_param_group_parse_atts($atts['item']);

            $output = sprintf("<div class='uk-grid uk-child-width-1-%s@m %s' uk-grid>",$atts['grid'],$class);

            for ($i=0; $i < count($items); $i++) {

                $link = vc_build_link($items[$i]['button']);

                $url = wp_get_attachment_image_src( $items[$i]['image'], 'full', '' )[0];

                $subdata = vc_param_group_parse_atts($items[$i]['item']);

                $data = '<ul>';
                for ($j=0; $j < count($subdata); $j++)
                $data .= '<li><span uk-icon="icon: check; ratio: 1"></span> '.$subdata[$j]['info'].'</li>';
                $data .= '</ul>';

                $output .= sprintf('
                    <div class="uk-text-center">
                        <div class="gerege-product">
                            <div class="thumbnail" style="background-image:url(%s);"></div>
                            <div class="uk-padding-small">
                                <h2 class="title">%s</h2>
                                <h5 class="price">%s</h5>
                                <p>%s</p>
                                <div class="uk-text-center">
                                    <a href="%s"><button class="primary-button">%s</button></a>
                                    %s
                                </div>
                            </div>
                        </div>
                    </div>
                ',$url,$items[$i]['title'],$items[$i]['price'],$items[$i]['desc'],$link['url'],$link['title'],$data);
            }

            $output .= '</div>';

            return $output;

        }


 
        public static function map() {
            return array(
                'name'        => esc_html__( 'Бүтээгдэхүүн', 'gerege' ),
                'description' => esc_html__( 'Гэрэгэ киоск болон пос машины төрөл', 'gerege' ),
                'base'        => 'gerege_product',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                    'type' => 'param_group',
                    'value' => '',
                    'param_name' => 'item',
                    'params' => array(
                        array(
                            'type' => 'textfield',
                            'value' => '',
                            'heading' => 'Title',
                            'param_name' => 'title',
                        ),
                        array(
                            'type' => 'textfield',
                            'value' => '',
                            'heading' => 'Price',
                            'param_name' => 'price',
                        ),
                        array(
                            'type' => 'textarea',
                            'value' => '',
                            'heading' => 'Descripton',
                            'param_name' => 'desc',
                        ),
                        array(
                            "type" => "attach_image",
                            "heading" => __( "Image", "gerege" ),
                            "param_name" => "image",
                        ),
                        array(
                            'type'       => 'vc_link',
                            'heading'    => esc_html__( 'button', 'gerege' ),
                            'param_name' => 'button',
                            'value'      => array()
                        ),
                        array(
                            'type' => 'param_group',
                            'value' => '',
                            'heading' => 'List',
                            'param_name' => 'item',
                            'params' => array(
                                array(
                                    'type' => 'textfield',
                                    'value' => '',
                                    'heading' => 'info',
                                    'param_name' => 'info',
                                ),
                            )
                        )
                    )
                ),
                array(
                    'type'          => 'dropdown',
                    'heading'       => __( 'Column', 'gerege' ),
                    'value'         => array(
                      __( '2 column', 'gerege' )    => '2',
                      __( '3 column', 'gerege' )    => '3',
                      __( '4 column', 'gerege' )    => '4',
                    ),
                    'param_name'    => 'grid'
                ),
                array(
                    "type" => "textfield",
                    "heading" => __( "Extra Class", "gerege" ),
                    "param_name" => "class",
                ),
            ),
            );
        }

    }

}
new gerege_product_Shortcode;
