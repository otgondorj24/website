<?php 

if ( ! class_exists( 'gerege_questions_Shortcode' ) ) {

    class gerege_questions_Shortcode {

        public function __construct() {

            add_shortcode( 'gerege_questions', __CLASS__ . '::output' );

            if ( function_exists( 'vc_lean_map' ) ) {
                vc_lean_map( 'gerege_questions', __CLASS__ . '::map' );
            }

        }


        public static function output( $atts, $content = null ) {

            $atts = vc_map_get_attributes( 'gerege_questions', $atts );

            if ($atts['style'] == 1) $class = $atts['class'] . ' standart';
            else $class = $atts['class'] . ' left-quest';

            $items = vc_param_group_parse_atts($atts['item']);

            $output = sprintf("<div class='gerege-questions %s'>",$class);

            $output .= '<h3 class="title">'.$atts['title'].'</h3>';

            for ($i=0; $i < count($items); $i++) {

                $output .= '<ul uk-accordion>
                    <li>
                        <a class="uk-accordion-title" href="#">'.$items[$i]["title"].'</a>
                        <div class="uk-accordion-content">
                            <p>'.$items[$i]["desc"].'</p>
                        </div>
                    </li>
                </ul>';

            }

            $output .= '</div>';

            return $output;

        }


 
        public static function map() {
            return array(
                'name'        => esc_html__( 'Түгээмэл асуулт', 'gerege' ),
                'description' => esc_html__( 'Хэрэглэгчийн түгээмэл асуулт хариулт', 'gerege' ),
                'base'        => 'gerege_questions',
                'category'    => 'Gerege Systems',
                'params'      => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Title',
                        'param_name' => 'title',
                    ),
                    array(
                    'type' => 'param_group',
                    'value' => '',
                    'param_name' => 'item',
                    'params' => array(
                        array(
                            'type' => 'textfield',
                            'value' => '',
                            'heading' => 'Title',
                            'param_name' => 'title',
                        ),
                        array(
                            'type' => 'textarea',
                            'value' => '',
                            'heading' => 'Descripton',
                            'param_name' => 'desc',
                        ),
                        )
                    ),
                array(
                    'type'          => 'dropdown',
                    'heading'       => __( 'Column', 'gerege' ),
                    'value'         => array(
                      __( 'Style 1', 'gerege' )    => '1',
                      __( 'Style 2', 'gerege' )    => '2',
                    ),
                    'param_name'    => 'style'
                ),
                array(
                    "type" => "textfield",
                    "heading" => __( "Extra Class", "gerege" ),
                    "param_name" => "class",
                ),
            ),
            );
        }

    }

}
new gerege_questions_Shortcode;
