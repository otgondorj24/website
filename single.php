<?php  get_header(); ?>
    <?php
    while ( have_posts() ) :
        the_post();
    ?>
    <div class="page-thumbnail" style="background-image:url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>)">
        <div class="title uk-position-bottom-center">
            <h5>
                <?php if (get_bloginfo("language") == 'mn') { ?>
                    <?php _e( 'Мэдээ мэдээлэл', 'gerege' ); ?>
                <?php
                } else {
                    _e( 'News', 'gerege' );
                } ?>
            </h5>
            <h2><?php echo get_the_title(); ?></h2>
        </div>
    </div>
    <div class="uk-container uk-container-small uk-margin-large-top uk-margin-large-bottom">
        <?php the_content(); ?>
        <div class="post-footer">
            <div class="uk-flex uk-flex-middle uk-child-width-1-2">
                <div class="author">
                    By <span><?php echo get_the_author(); ?></span> &nbsp;&nbsp; | &nbsp;&nbsp; <?php echo get_the_date( 'M d,Y', get_the_ID() ) ?>
                </div>
                <div class="uk-text-right">
                    <a class="share-button" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()) ?>">
                        <?php if (get_bloginfo("language") == 'mn') { ?>
                            <?php _e( 'Хуваалцах', 'gerege' ); ?>
                        <?php
                        } else {
                            _e( 'Share', 'gerege' );
                        } ?>
                    <span uk-icon="icon: social; ratio: 1"></span></a>
                </div>
            </div>
        </div>
    </div>
    <?php endwhile; ?>

    <div class="uk-container recent-post">
        <h2>
            <?php if (get_bloginfo("language") == 'mn') { ?>
                    <?php _e( 'Бусад мэдээлэл', 'gerege' ); ?>
                <?php
                } else {
                    _e( 'Recent news', 'gerege' );
                } ?>
        </h2>
        <div class="uk-grid uk-margin-medium-top uk-child-width-1-3@m" uk-grid>
            <?php
            $recent_posts = wp_get_recent_posts(array(
                'numberposts' => 3,
                'post_status' => 'publish'
            ));
            foreach( $recent_posts as $post_item ) : ?>
                    <div class='gerege-post'>
                        <div class='thumbnail' style='background-image:url("<?php echo get_the_post_thumbnail_url($post_item['ID'], 'full'); ?>")'></div>
                        <h4><?php echo $post_item['post_title']; ?></h4>
                        <div class='uk-margin-medium-top uk-grid uk-child-width-1-2' uk-grid>
                        <div><a href='<?php echo get_permalink($post_item['ID']); ?>'><button class='primary-button'>
                            <?php if (get_bloginfo("language") == 'mn') { ?>
                                <?php _e( 'Дэлгэрэнгүй', 'gerege' ); ?>
                            <?php
                            } else {
                                _e( 'Read more', 'gerege' );
                            } ?>
                        </button></a></div>
                        <div class='uk-text-right date'><?php echo get_the_date( 'Y/m/d',$post_item['ID'] ); ?></div>
                        </div>
                    </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php get_footer(); ?>
