    <footer>
        <div class="uk-container uk-margin-large-top">
            <div class="uk-grid uk-child-width-1-4@m" uk-grid>
                <div>
                    <a href="<?php echo get_home_url(); ?>">
                        <?php if (get_bloginfo("language") == 'mn') {
                        ?>
                        <img src="<?php echo get_template_directory_uri() .'/assets/images/wlogo.png'; ?>" class="footer-logo" />
                        <?php
                        } else {

                        } ?>
                    </a>
                </div>
                <div>
                    <?php
                        if ( is_active_sidebar( 'footer-1' ) ) {
                            dynamic_sidebar( 'footer-1' );
                        }
                    ?>
                </div>
                <div>
                    <?php
                        if ( is_active_sidebar( 'footer-2' ) ) {
                            dynamic_sidebar( 'footer-2' );
                        }
                    ?>
                </div>
                <div>
                    <?php
                        if ( is_active_sidebar( 'footer-3' ) ) {
                            dynamic_sidebar( 'footer-3' );
                        }
                    ?>
                </div>
            </div>
            <div class="copywrite">
                <?php
                    if ( is_active_sidebar( 'footer-4' ) ) {
                        dynamic_sidebar( 'footer-4' );
                    }
                ?>
            </div>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-I2gxoes7K0AZxH6p4mCWXQ6k7U2yTNU&callback=initMap" defer></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/uikit.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/uikit-icons.min.js"></script>
    <?php wp_footer(); ?>
</body>
</html>