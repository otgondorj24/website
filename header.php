<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/uikit.min.css" />
		<?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="loader">
	<div class="uk-position-center">
		<div class="radar-type1">
			<span class="cir-zero"></span>
			<span class="cir-one"></span>
			<span class="cir-two"></span>
			<span class="cir-three"></span>
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/load.png" />
	</div>
</div>
<header uk-sticky="start: 200; animation: uk-animation-slide-top">
	<div class="uk-flex uk-flex-middle uk-visible@m">
		<div class="uk-width-auto logo">
			<a href="<?php echo get_home_url(); ?>">
				<?php if (get_bloginfo("language") == 'mn') {
				?>
				<img src="<?php echo get_template_directory_uri() .'/assets/images/blogo.png'; ?>" />
				<?php
				} else {

				} ?>
			</a>
		</div>
		<div class="uk-width-expand uk-flex uk-flex-right">
			<?php
				wp_nav_menu(
					array(
						'menu_class' => 'primary-menu',
						'theme_location' => 'primary',
					)
				);
			?>
			<div class="search">
				<a href="#search-modal" uk-toggle><span uk-icon="icon: search; ratio: 1" ></span></a>
			</div>
		</div>
	</div>
	<div class="uk-flex uk-flex-middle uk-padding-small uk-hidden@m responsive-menu">
		<div class="uk-width-auto logo uk-margin-remove">
			<a href="<?php echo get_home_url(); ?>">
				<img src="<?php echo get_template_directory_uri() .'/assets/images/blogo.png'; ?>" />
			</a>
		</div>
		<div class="uk-width-expand uk-flex uk-flex-right">
			<span uk-toggle="target: #responsive-menu" uk-icon="icon: menu; ratio: 1.7" ></span>
		</div>
	</div>
</header>
<div id="search-modal" class="uk-modal-full" uk-modal>
    <div class="uk-modal-dialog search-body">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        <div class="uk-position-center">
			<?php get_search_form(); ?>
		</div>
    </div>
</div>
<div id="responsive-menu" uk-offcanvas="mode: push; overlay: true; ">
	<div class="uk-offcanvas-bar">
		<button class="uk-offcanvas-close" type="button" uk-close></button>
		<div class="uk-margin-medium-top">
			<a href="<?php echo get_home_url(); ?>">
				<?php if (get_bloginfo("language") == 'mn') {
				?>
				<img src="<?php echo get_template_directory_uri() .'/assets/images/wlogo.png'; ?>" />
				<?php
				} else {

				} ?>
			</a>
			<?php
				wp_nav_menu(
					array(
						'menu_class' => 'responsive-ul-menu uk-margin-top',
						'theme_location' => 'primary',
					)
				);
			?>
			<div class="uk-margin-medium-top">
			<div class="uk-margin">
				<form class="uk-search uk-search-default" method="get">
					<span class="uk-search-icon-flip" uk-search-icon></span>
					<input class="uk-search-input" type="search" placeholder="Хайлт..." value="<?php echo get_search_query(); ?>" name="s">
				</form>
			</div>
			</div>
			<div class="uk-position-bottom uk-margin-medium-bottom uk-text-center switch-language">
				<a href="#">ENG</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="#">MON</a>
			</div>
		</div>
	</div>
</div>