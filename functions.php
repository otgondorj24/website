<?php

function gerege_theme_support() {

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );

}

add_action( 'after_setup_theme', 'gerege_theme_support' );

require_once get_template_directory() . '/elements/button.php';
require_once get_template_directory() . '/elements/video.php';
require_once get_template_directory() . '/elements/organization.php';
require_once get_template_directory() . '/elements/blog.php';
require_once get_template_directory() . '/elements/slider.php';
require_once get_template_directory() . '/elements/product.php';
require_once get_template_directory() . '/elements/kiosk-location.php';
require_once get_template_directory() . '/elements/questions.php';
require_once get_template_directory() . '/elements/team.php';
require_once get_template_directory() . '/elements/timeline.php';
require_once get_template_directory() . '/elements/testimonial.php';
require_once get_template_directory() . '/elements/workspace.php';
require_once get_template_directory() . '/elements/map.php';

/** Kiosk Data */
require get_template_directory() . '/location/location.php';


function theme_enqueue_styles() {
 
    wp_enqueue_style( 'gerege-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'main-css', get_template_directory_uri() . '/assets/css/main.css' );

}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );


function gerege_menu() {

	$locations = array(
		'primary'  => __( 'Primary Menu', 'gerege' ),
		'footer'   => __( 'Footer Menu', 'gerege' ),
	);

	register_nav_menus( $locations );
}

add_action( 'init', 'gerege_menu' );


// if ( ! function_exists( 'wp_body_open' ) ) {

// 	function wp_body_open() {
// 		do_action( 'wp_body_open' );
// 	}

// }

// add_action( 'wp_body_open', 'gerege_skip_link', 5 );


function gerege_main_enqueue_scripts() {

	$theme_version = wp_get_theme()->get( 'Version' );

	wp_enqueue_script( 'gerege-main-script', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), $theme_version, true );

}

add_action( 'wp_enqueue_scripts', 'gerege_main_enqueue_scripts' );

function gerege_widgets_init() {

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Column 1', 'gerege' ),
			'id'            => 'footer-1',
			'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'gerege' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Column 2', 'gerege' ),
			'id'            => 'footer-2',
			'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'gerege' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Column 3', 'gerege' ),
			'id'            => 'footer-3',
			'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'gerege' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Copywrite text', 'gerege' ),
			'id'            => 'footer-4',
			'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'gerege' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
		)
	);
}
add_action( 'widgets_init', 'gerege_widgets_init' );