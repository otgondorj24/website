<?php  get_header(); ?>

<div class="uk-container archive">
    <div class="emp100"></div>
    <div><h1 class="page-title">
        <?php if (get_bloginfo("language") == 'mn') { ?> Мэдээ мэдээлэл
        <?php } else { ?>
            News
        <?php } ?>
    </h1></div>
    <div class="emp50"></div>
    <?php
    $first = true;
    $blog = '';
    while ( have_posts() ) :
        the_post();
        if ($first) {
            ?>
            <div class="uk-grid" uk-grid>
                <div class="uk-width-2-5@m">
                    <div class="single-picture" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>'); width:100%; height:500px;">
                    </div>
                </div>
                <div class="uk-width-3-5@m uk-flex uk-flex-middle">
                    <div class="ml-40">
                        <h2 class="archive-title uk-margin-remove-top"><?php echo get_the_title(); ?></h2>
                        <p class="uk-margin-medium-top uk-margin-medium-bottom"><?php echo get_the_excerpt(); ?></p>
                        <a href="<?php echo get_the_permalink(); ?>">
                        <button class='primary-button'>
                            <?php if (get_bloginfo("language") == 'mn') { ?>
                                Дэлгэрэнгүй
                            <?php } else { ?>
                                Read more
                            <?php } ?>
                        </button>
                        </a>
                    </div>
                </div>

            </div>
            <div class="emp100"></div>
            <div class="emp100"></div>
            <?php
        } else {
              $blog .="<div class='gerege-post'>";
              $blog .="<div class='thumbnail' style='background-image:url(".get_the_post_thumbnail_url(get_the_ID(),'full').")'></div>";
              $blog .="<h4>".get_the_title()."</h4>";
              $blog .="<div class='uk-margin-medium-top uk-grid uk-child-width-1-2' uk-grid>";
              $blog .="<div><a href='".get_the_permalink()."'><button class='primary-button'>";
              if (get_bloginfo("language") == 'mn') $blog .= 'Дэлгэрэнгүй';
              else $blog .= 'Read more';
              $blog .="</button></a></div>";
              $blog .="<div class='uk-text-right date'>".get_the_date( 'Y/m/d', get_the_ID() )."</div>";
              $blog .="</div>";
              $blog .="</div>";
        }
        $first = false;
    endwhile; 
    ?>
    <div class="uk-grid uk-child-width-1-3@m" uk-grid>
        <?php echo $blog; ?>
    </div>
    <div class="emp100"></div>
    <div class="pagination">
        <?php
        if (get_bloginfo("language") == 'mn') $args = array(
            'format'             => '?paged=%#%',
            'prev_next'          => true,
            'prev_text'          => __('« Өмнөх'),
            'next_text'          => __('Дараах »'),
            'type'               => 'list',
            'add_args'           => false,
            'add_fragment'       => '',
            'before_page_number' => '',
            'after_page_number'  => ''
        );
        else $args = array(
            'format'             => '?paged=%#%',
            'prev_next'          => true,
            'prev_text'          => __('« Previous'),
            'next_text'          => __('Next »'),
            'type'               => 'list',
            'add_args'           => false,
            'add_fragment'       => '',
            'before_page_number' => '',
            'after_page_number'  => ''
        );
        echo paginate_links($args); 
        ?>
    </div>
    <div class="emp100"></div>
    <div class="emp100"></div>
</div>

<?php get_footer(); ?>
