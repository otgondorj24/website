<?php  get_header(); ?>
    <div class="uk-container search-page uk-margin-large-bottom">
        <?php
        $s=get_search_query();
        $args = array(
            's' => $s,
            'post_type' => 'post'
        );
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) {
                ?>
                <div class="uk-margin-large-top search-result-title uk-margin-large-bottom">
                    <h1>
                        <?php if (get_bloginfo("language") == 'mn') {
                        ?>
                            Хайлтын утга: 
                        <?php
                        } else {
                            Search result:
                        } ?>
                        <span><?php echo get_query_var('s'); ?></span>
                    </h1>
                </div>
                <?php
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                        ?>
                            <div class="uk-grid uk-grid-collapse gerege-posts" uk-grid>
                                <div class="uk-width-2-5@m">
                                    <div class="thumb" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>');"></div>
                                </div>
                                <div class="uk-width-3-5@m">
                                    <div class="excerp">
                                        <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                                        <p>
                                            <?php echo get_the_excerpt(); ?>
                                        </p>
                                        <a href="<?php echo get_the_permalink(); ?>">
                                        <?php if (get_bloginfo("language") == 'mn') {
                                        ?>
                                            <button class='primary-button'>Дэлгэрэнгүй</button>
                                        <?php
                                        } else { ?>
                                            <button class='primary-button'>Read more</button>
                                        <?php } ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php
                }
                ?>
                <div class="emp100"></div>
                <div class="pagination">
                    <?php 
                    if (get_bloginfo("language") == 'mn') { 
                        $args = array(
                            'format'             => '?paged=%#%',
                            'prev_next'          => true,
                            'prev_text'          => __('« Өмнөх'),
                            'next_text'          => __('Дараах »'),
                            'type'               => 'list',
                            'add_args'           => false,
                            'add_fragment'       => '',
                            'before_page_number' => '',
                            'after_page_number'  => ''
                        );
                    } else {
                        $args = array(
                            'format'             => '?paged=%#%',
                            'prev_next'          => true,
                            'prev_text'          => __('« Previous'),
                            'next_text'          => __('Next »'),
                            'type'               => 'list',
                            'add_args'           => false,
                            'add_fragment'       => '',
                            'before_page_number' => '',
                            'after_page_number'  => ''
                        );
                    }
                    echo paginate_links($args); 
                    ?>
                </div>
                <?php
            } else {
                ?>
                <div class="uk-margin-large-top search-result-title">
                    <h1>
                        <?php if (get_bloginfo("language") == 'mn') { ?>
                            Мэдээлэл олдсонгүй
                        <?php
                        } else { ?>
                            Not search result
                        <?php } ?>
                    </h1>
                </div>
                <div class="uk-margin-top uk-text-center">
                    <img class="uk-margin-medium-top" src="<?php echo get_template_directory_uri() .'/assets/images/no-data.webp'; ?>" />
                    <p class="uk-margin-medium-bottom">
                        <?php if (get_bloginfo("language") == 'mn') { ?>
                            Уучлаарай, Таны хайсан түлхүүр үгд нийцэх мэдээлэл олдсонгүй <br>та өөр түлхүүр үг ашиглана уу!
                        <?php
                        } else {

                        } ?>
                    </p>
                </div>
        <?php } ?>
    </div>
    <div class="emp100"></div>
<?php get_footer(); ?>
