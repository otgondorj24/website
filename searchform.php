<?php
    $gerege_unique_id = wp_unique_id( 'search-form-' );
    $gerege_aria_label = ! empty( $args['aria_label'] ) ? 'aria-label="' . esc_attr( $args['aria_label'] ) . '"' : '';
?>
<form role="search" <?php echo $gerege_aria_label; ?> method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div>
        <label for="<?php echo esc_attr( $gerege_unique_id ); ?>">
            <h1>
                <?php if (get_bloginfo("language") == 'mn') { ?>
                    <?php _e( 'Мэдээлэл хайх', 'gerege' ); ?>
                <?php
                } else {

                } ?>
            </h1>
        </label>
        <div class="search-input">
            <div class="uk-flex">
                <div class="uk-width-auto">
                    <span uk-icon="icon: search; ratio: 1.5" ></span>
                </div>
                <div class="uk-width-expand">
                    <?php if (get_bloginfo("language") == 'mn') { ?>
                        <input type="search" class="search-input" id="<?php echo esc_attr( $gerege_unique_id ); ?>" placeholder="Хайлтын утга..." value="<?php echo get_search_query(); ?>" name="s" />
                    <?php
                    } else { ?>
                        <input type="search" class="search-input" id="<?php echo esc_attr( $gerege_unique_id ); ?>" placeholder="Search text..." value="<?php echo get_search_query(); ?>" name="s" />
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php if (get_bloginfo("language") == 'mn') { ?>
            <input type="submit" class="search-submit uk-margin-medium-top" value="<?php echo esc_attr_x( 'Хайх', '', 'gerege' ); ?>" />
        <?php
        } else { ?>
             <input type="submit" class="search-submit uk-margin-medium-top" value="<?php echo esc_attr_x( 'Search', '', 'gerege' ); ?>" />
        <?php } ?>
    </div>
</form>
